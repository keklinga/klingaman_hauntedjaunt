﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    //Access the player's transform component
    public Transform player;
    //References the game ending class
    public GameEnding gameEnding;
    //Variable stores if the player has crossed into the gargoyle's sight
    bool m_IsPlayerInRange;


    //Checks if gameobject has collided with another and performs what's in the body
    void OnTriggerEnter(Collider other)
    {
        //Checks if the gameobject that entered was the player
        if (other.transform == player)
        {
            //Player is in range
            m_IsPlayerInRange = true;
        }
    }

    //Determines if gameobject has exited the trigger
    void OnTriggerExit(Collider other)
    {
        //Checks if the gameobject that exited was the player
        if (other.transform == player)
        {
            //Player is no longer in range
            m_IsPlayerInRange = false;
        }
    }

    //Updates once per frame
    void Update()
    {
        //Checks the line of sight when the player is in range
        if (m_IsPlayerInRange)
        {
            //Stores direction for POV gameobject to John Lemon
            Vector3 direction = player.position - transform.position + Vector3.up;
            //Creates a new ray from the position of the POV gameobject
            Ray ray = new Ray(transform.position, direction);

            //Stores whatever the raycast has hit
            RaycastHit raycastHit;
            //Checks if there is a ray cast being projected and sets the raycast out parameter to whatever it hit
            if (Physics.Raycast(ray, out raycastHit))
            {
                //Checks if the player has been hit by the raycast
                if (raycastHit.collider.transform == player)
                {
                    //Access the PlayerMovement script object
                    PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();

                    //Checks if the player is not invulnerable
                    if(!playerMovement.invulnerable)
                        //Initiates CaughtPLayer sequence
                        gameEnding.CaughtPlayer();
                }
            }
        }
    }
}
