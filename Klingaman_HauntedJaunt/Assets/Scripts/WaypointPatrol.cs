﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.UI;
public class WaypointPatrol : MonoBehaviour
{
    //Access the NavMeshAgent component
    public NavMeshAgent navMeshAgent;
    //Stores an array of waypoints for the ghost
    public Transform[] waypoints;
    //Stores integer variable for the current waypoint
    int m_CurrentWaypointIndex;
    //Creates boolean to store if the direction has been flipped
    bool flippedDirection = false;
    //Stores how many times the player can flip directions
    int flipDirectionCharges = 2;
    //Creates a reference to access the flip charges text
    public TextMeshProUGUI flipChargeText;
    //Access flip charge slider
    public Slider flipChargeBar;

    // Start is called before the first frame update
    void Start()
    {
        //Sets the initial destination of the ghost
        navMeshAgent.SetDestination(waypoints[0].position);
        //Sets the text for how many flip charges the player has
        SetFlipDirectionText();
        //Sets the initial flip charge bar
        SetFlipBar();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Checks if Tab has been pressed and if they have enough charges to use ability
        if (Input.GetKeyDown(KeyCode.Tab) && flipDirectionCharges > 0)
        {
            //Changes the direction of the enemy
            FlipDirection();
            //Decreases the counter for how many times they can change the direction
            flipDirectionCharges--;
            //Adjusts the flip charge bar if it gets used
            SetFlipBar();
            //Sets the text for how many charges remain to flip the guards
            SetFlipDirectionText();
        }

        //Checks if the remaining distance of the enemies navmesh is less than or equal to it's stopping distance.
        if(navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
        {
            //If it is, then it sets the next waypoint for the enemy to go to
            SetNextWaypoint();
        }
    }

    //Function to change the direction of the enemy
    public void FlipDirection()
    {
        //Sets the boolean to the opposite o what it currently is
        flippedDirection = !flippedDirection;
        //Sets the next waypoint of the enemy
        SetNextWaypoint();
    }

    //Sets a flip charge bar for the player to see how many charges they have left
    void SetFlipBar()
    {
        //Assigns the slider value to the amount of charges the player has
        flipChargeBar.value = flipDirectionCharges;
    }

    //Function to set the next waypoint/direction of the enemy
    void SetNextWaypoint()
    {
        //Checks if the boolean is true
        if (flippedDirection)
        {
            //Reassigns the current waypoint index
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex - 1);
            //Checks if the index is now less than 0
            if (m_CurrentWaypointIndex < 0)
            {
                //If it is, reassign it to be 1 less than the length of the waypoints array
                m_CurrentWaypointIndex = waypoints.Length - 1;
            }
        }

        else
            //Reassigns the current waypoint index
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
        //Otherwise, Sets the destination of the enemy to the waypoints position
        navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
    }

    
    void SetFlipDirectionText()
    {
        //Sets the flip charges's text
        flipChargeText.text = "Flip Charges: " + flipDirectionCharges.ToString();
    }
}
