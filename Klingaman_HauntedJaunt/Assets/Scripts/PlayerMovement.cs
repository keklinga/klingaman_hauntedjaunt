﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class PlayerMovement : MonoBehaviour
{
    //Initialized turn speed variable 
    public float turnSpeed = 20f;
    //Creates an animator variable to reference
    Animator m_Animator;
    //Creates a rigidbody variable to reference
    Rigidbody m_Rigidbody;
    //Creates an audio source reference
    AudioSource m_AudioSource;
    //Create vector3 variable to store movement data
    Vector3 m_Movement;
    //Create rotation variable to store the rotation
    Quaternion m_Rotation = Quaternion.identity;
    //Creates bool to store if the player is invulnerable
    public bool invulnerable;
    //Creates a variable to store how much time the invulnerability lasts
    float invulnerableCooldown;
    //Creates a renderer variable to reference
    public Renderer renderer;
    //Creates a material variable to reference the PBR material
    public Material matJohnPBR;
    //Creates a material variable to reference the Toon material
    public Material matJohnToon;
    //Creates a material variable to referencce the Transparent material
    public Material matJohnTransparent;
    //Creates variable to store how many charges the player has
    int invisibilityCharges = 3;
    //Access the invis charge text
    public TextMeshProUGUI invisChargeText;
    //Access the timer text
    public TextMeshProUGUI invulnerableTimer;
    //Access the slider for the invisibility charges
    public Slider invisChargeBar;

    // Start is called before the first frame update
    void Start()
    {
        //Access the animator component
        m_Animator = GetComponent<Animator>();
        //Access the rigidbody component
        m_Rigidbody = GetComponent<Rigidbody>();
        //Access the audio source component
        m_AudioSource = GetComponent<AudioSource>();
        //Sets the amount of invis/invulnerability charges the player has
        SetInvisChargeText();
        //Sets the text to be disabled at the start
        invulnerableTimer.enabled = false;
        //Initiates the invis charge bar
        SetInvisBar();

    }

    void Update()
    {
        //Checks if the player is in invulnerability mode
        if (invulnerable)
        {

            //Decreases the timer 
            invulnerableCooldown -= Time.deltaTime;
            //Displays the cooldown for the invis/invulnerability as an int
            invulnerableTimer.text = "Invisibility Remaining: " + Mathf.RoundToInt(invulnerableCooldown);
            //Checks if the timer has run out for invulnerability
            if (invulnerableCooldown <= 0f)
            {
                //Sets the boolean to false;
                invulnerable = false;
                //Sets the text to be disabled after the cooldown runs out of time
                invulnerableTimer.enabled = false;
                //Creates an array to store the original materials of the player model
                Material[] mats = new Material[] { matJohnToon, matJohnPBR };
                //Reassigns the original materials of the player model
                renderer.materials = mats;
            }
        }
        //Checks if Space has been pressed, there isn't a cooldown, and they still have charges left
        if (Input.GetKeyDown(KeyCode.Space) && invulnerableCooldown <= 0f && invisibilityCharges > 0)
        {
            //Sets boolean to true
            invulnerable = true;
            //Sets timer to 3 seconds
            invulnerableCooldown = 3f;
            //Decreases invulnerability charges by 1
            invisibilityCharges--;
            //Decreases the slider bar when the player uses a charge
            SetInvisBar();
            //Sets the amount of invis/invulnerability charges the player has
            SetInvisChargeText();
            //Enables the timer to show how much time they have while using the invis/invulnerability
            invulnerableTimer.enabled = true;
            //Creates an array to store the invisible material for the player model
            Material[] mats = new Material[] { matJohnTransparent, matJohnTransparent };
            //Reassigns the materials of the player to be the Transparent materials
            renderer.materials = mats;


        }
    }

    //Sets a invis charge bar for the player to see how many charges they have left
    void SetInvisBar()
    {
        //Assigns the slider value to the amount of charges the player has
        invisChargeBar.value = invisibilityCharges;
    }


    void FixedUpdate()
    {
        //Stores a float for the direction found at GetAxis horizontally
        float horizontal = Input.GetAxis("Horizontal");
        //Stores a float for the direction found at GetAxis vertically
        float vertical = Input.GetAxis("Vertical");

        //Updates movement every frame based off the input from the player
        m_Movement.Set(horizontal, 0f, vertical);
        //Keeping the vector's direction but keeping magnitude at 1
        m_Movement.Normalize();

        //Determines if there is horizontal input
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        //Determines if there is vertical input
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        //Checks if the player has horizontal or vertical input and then returns true or false
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        //Sets the animator parameter
        m_Animator.SetBool("IsWalking", isWalking);
        //Checks if the player is walking
        if (isWalking)
        {
            //Checks if the audio source is not already playing
            if (!m_AudioSource.isPlaying)
            {
                //Plays the audio source
                m_AudioSource.Play();
            }
        }
        else
        {
            //Stops playing the audio source
            m_AudioSource.Stop();
        }

        //Controls the turn/rotation speed of the player to keep movement in the desired direction
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);

        //Creates rotation that looks at the given direction
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    //Allows us to apply root motion as desired
    void OnAnimatorMove()
    {
        //Moves the character in the actual direction we want to move
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        //Directly sets the rotation
        m_Rigidbody.MoveRotation(m_Rotation);

    }

    private void OnTriggerEnter(Collider other)
    {
        //Checks if the gameobject is an InvisCharge
        if (other.gameObject.CompareTag("InvisCharge") && invisibilityCharges < 3)
        {
            //Deletes the gameObject from the scene after collision
            other.gameObject.SetActive(false);
            //Adds an extra InvisCharge to the player
            invisibilityCharges += 1;
            //Changes the bar to add 1 back to the invis charge count
            SetInvisBar();
            //Sets the amount of invis/invulnerability charges the player has
            SetInvisChargeText();
        }
    }

    //Function to set the text anywhere
    void SetInvisChargeText()
    {
        //Sets the invisibility charge's text
        invisChargeText.text = "Invisibility Charges: " + invisibilityCharges.ToString();
    }
}