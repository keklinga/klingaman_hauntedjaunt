﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    //Stores the duration to fade out the end screen
    public float fadeDuration = 1f;
    //Stores the duration to fade in the end screen
    public float displayImageDuration = 1f;
    //Access the player gameobject
    public GameObject player;
    //Access the canvas group exit component
    public CanvasGroup exitBackgroundImageCanvasGroup;
    //Access the exit audio component
    public AudioSource exitAudio;
    //Access the canvas group caught component to display if JohnLemon has been caught
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    //Access the caught audio component
    public AudioSource caughtAudio;
    //Variable to make sure audio plays once
    bool m_HasAudioPlayed;

    //Way of storing when to start the fading process if at the exit
    bool m_IsPlayerAtExit;
    //Way of storing when to start the end game process if caught
    bool m_IsPlayerCaught;
    //Stores value for timer variable
    float m_Timer;

    //Method to declare what happens when something collides with the end trigger
    void OnTriggerEnter(Collider other)
    {
        //Checks if the colliding object is the player
        if (other.gameObject == player)
        {
            //Sets the way of checking to fade to true
            m_IsPlayerAtExit = true;
        }

    }

    //Method to initiate the caught player sequence
    public void CaughtPlayer()
    {
        //Sets boolean variable to true
        m_IsPlayerCaught = true;
    }

    //Updates once per frame
    void Update()
    {
        //Checks if the player has reached the trigger
        if (m_IsPlayerAtExit)
        {
            //Initiates EndLevel method if at exit, and then quit the game
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        //If the player is not at the exit, check if they have been caught
        else if (m_IsPlayerCaught)
        {
            //Initiate EndLevel method if caught, and restart the game
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }


    //Method to create the end level sequence
    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        //Checks if the audio hasn't already played
        if (!m_HasAudioPlayed)
        {
            //Plays the audio
            audioSource.Play();
            //Changes the boolean to true so it doesn't keep playing
            m_HasAudioPlayed = true;
        }
        //Increases the timer variable
        m_Timer += Time.deltaTime;
        //Sets the alpha for the image to fade into based on the timer
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        //Way to tell the game to quit when the fade in and fade out are finished
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            //Checks if it needs to restart
            if (doRestart)
            {
                //Reloads the main scene to start the game over
                SceneManager.LoadScene(0);
            }
            else
            {
                //Quits the game, and closes it
                Application.Quit();
            }
        }
    }
}
